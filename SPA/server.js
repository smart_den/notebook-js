var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var http = require('http').Server(app);

var group =[
    {
        id:1,
        name:'Vetalb',
        lastName:'Motil',
    },
    {
        id:2,
        name:'Sanya',
        lastName:'Bokat',
    },

];

app.use(express.static(__dirname + '/public/'));
app.use('/second', express.static('public')) ;
app.use('/node_modules', express.static(__dirname + '/node_modules')) ;

http.listen(8000, function(){
    console.log('YES');
});

app.get("/getUser", function(request, response){
    response.send(group);
});

app.post("/", function(request, response){
    response.send("<h1>POST</h1>");
});